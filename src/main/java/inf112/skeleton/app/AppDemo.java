package inf112.skeleton.app;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import turtleduck.TurtleDuckApp;
import turtleduck.canvas.Canvas;
import turtleduck.colors.Colors;
import turtleduck.display.Screen;
import turtleduck.geometry.Direction;
import turtleduck.geometry.Orientation;
import turtleduck.geometry.Point;
import turtleduck.gl.GLLauncher;
import turtleduck.gl.GLLayer;
import turtleduck.gl.GLPixelData;
import turtleduck.gl.GLScreen;
import turtleduck.image.Image;
import turtleduck.image.ImageFactory;
import turtleduck.image.TileAtlas;
import turtleduck.paths.Pen;
import turtleduck.turtle.Turtle;
import turtleduck.turtle.BaseNavigator.RelativeTo;

/**
 * A demo application
 * 
 * @author anya
 *
 */
public class AppDemo implements TurtleDuckApp {
	/**
	 * The currently running demo
	 */
	private static AppDemo demo;
	/**
	 * How many frames we normally want to render per second
	 */
	private static final int DEFAULT_FPS = 20;
	/**
	 * Number of nanoseconds in a second
	 */
	private static final long NANOS_PER_SEC = 1_000_000_000;
	/**
	 * How many frames we want to render per second
	 */
	private int targetFps = DEFAULT_FPS;
	/**
	 * How many nanoseconds to spend per frame, in order to reach {@link #targetFps}
	 */
	private long nanosPerFrame = NANOS_PER_SEC / targetFps;

	/**
	 * The Screen we're drawing to
	 */
	private Screen screen;
	/**
	 * True if we're currently paused
	 */
	boolean paused;

	private Canvas canvas;
	private Pen pen;
	private Turtle turtle3;
	private Turtle turtle;
	private double scale = 0;
	private int step = 0;
	private Image image;
	private int fieldCount = 0;
	private int frameCount = 0;

	/**
	 * @return The currently running demo
	 */
	public static AppDemo getInstance() {
		return demo;
	}

	/**
	 * Start the application
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// new JfxLauncher().app(new TDDemo()).launch(args);
		new GLLauncher().app(new AppDemo()).launch(args);
	}

	private void unpause() {
		paused = false;
	}

	private void pause() {
		paused = true;
	}

	public Turtle arc(Turtle turtle, double angle, double arcLength) {
		double r = arcLength / Math.toRadians(angle);
		double crd = 2 * Math.sin(Math.toRadians(angle) / 2);
		turtle.spawn().turn(angle / 2).strokeWidth(3).color(Colors.RED).draw(r * crd);
//		turtle.turn(-angle / 2);
		int steps = 10;
		for (int i = 0; i < steps; i += 1) {
			turtle.turn(.5 * angle / steps).draw(arcLength / steps).turn(.5 * angle / steps);
		}
//		turtle.turn(-angle / 2);
		return turtle;
	}

	public Turtle arc2(Turtle turtle, double angle, double chordLength) {

		double crd = 2 * Math.sin(Math.toRadians(angle) / 2);
		double r = chordLength / crd;
		double arcLength = Math.toRadians(angle) * r;
		turtle.spawn().turn(angle / 2).strokeWidth(3).color(Colors.RED).draw(chordLength);
//		turtle.turn(-angle / 2);
		int steps = 10;
		for (int i = 0; i < steps; i += 1) {
			turtle.turn(.5 * angle / steps).draw(arcLength / steps).turn(.5 * angle / steps);
		}
//		turtle.turn(-angle / 2);
		return turtle;
	}

	public Turtle arcTo(Turtle turtle, double angle, Point to) {
		double chordLength = turtle.point().distanceTo(to);
		if (chordLength < 0.0001)
			return turtle;
		Direction dirT = turtle.direction().yaw(180);
		Direction dirP = turtle.point().directionTo(to);
		double a = dirT.sub(dirP).degrees();
		angle = 2 * a;
//		return arc2(turtle.turnTo(turtle.point().directionTo(to)), angle, dist).turn(angle/2);
		double crd = 2 * Math.sin(Math.toRadians(angle) / 2);
		double r = chordLength / crd;
		double arcLength = Math.toRadians(angle) * r;
		turtle.spawn().strokeWidth(3).color(Colors.RED).draw(chordLength);
		turtle.spawn().strokeWidth(3).color(Colors.GREEN).drawTo(to);
		int steps = 10;
		for (int i = 0; i < steps; i += 1) {
			turtle.turn(.5 * angle / steps).draw(arcLength / steps).turn(.5 * angle / steps);
		}
		return turtle;
	}

	public Turtle drawBezier(Turtle turtle, double ctrl1Dist, double ctrl2Dist, double ctrl2Angle, Point dest) {
		Point ctrl2 = dest.add(Direction.absolute(-ctrl2Angle), ctrl2Dist);
		return drawBezier(turtle, ctrl1Dist, ctrl2, dest);

	}

	public Turtle drawBezier(Turtle turtle, double ctrl1Dist, Point ctrl2, Point dest) {
		Point ctrl1 = turtle.spawn().jump(ctrl1Dist).point();
		Point src = turtle.point();
		double n = 20;
		for (int i = 0; i <= n; i++) {
			double f = i / n;
			Point p1 = src.interpolate(ctrl1, f);
			Point p2 = ctrl1.interpolate(ctrl2, f);
			Point p3 = ctrl2.interpolate(dest, f);
			Point p4 = p1.interpolate(p2, f);
			Point p5 = p2.interpolate(p3, f);
			Point p6 = p4.interpolate(p5, f);
			turtle.drawTo(p6);
		}
		turtle.endPath();
		return turtle;
	}

	List<Point> bezierPoints = new ArrayList<>();
	int bpPos = 0;
	private TileAtlas atlas;

	public void redraw() {
//		canvas.color(Colors.GRAY.opacity(0.3)).rectangle().at(Point.point(0, 0)).width(1280).height(720).stroke();
		canvas.clear();
		for (int i = 0; i < 360; i++) {
			Point point2 = Point.ZERO.add(Direction.absolute(i % 360), 300);
			drawBezier(turtle.spawn().color(Colors.WHITE).jumpTo(0, 0).turnTo(i % 360), 200, 200, i % 360, point2);
		}
	}

	protected void drawFrame() {
		int x = -600, y = 350;
		for (int i = 0; i < atlas.size(); i++) {
			Image tile = atlas.get(i);
//			System.out.print(tile + "  ");
			(((GLScreen)screen).getGLLayer()).drawImage(Point.point(x, y), tile);
			y-=75;
			if (y < -350) {
				x += 75;
				y = 350;
//				System.out.println();
			}
		}

		turtle.color(Colors.WHITE).strokeOnly();
		turtle.jumpTo(400, 150).turnTo(-90);
		drawSnowFlake(turtle, 50, 0, 3);
		turtle.jumpTo(200, -150).turnTo(-90);
		drawSnowFlake(turtle, 50, 1, 3);
		turtle.jumpTo(0, 150).turnTo(-90);
		drawSnowFlake(turtle, 50, 2, 4);
		turtle.jumpTo(-200, -150).turnTo(-90);
		drawSnowFlake(turtle, 50, 3, 3);
		
		for (int i = 0; i < 360; i++) {
			Point point2 = Point.ZERO.add(Direction.absolute(i % 360), 300);
			drawBezier(turtle.spawn().color(Colors.WHITE).jumpTo(0, 0).turnTo(i % 360), 200, 200, i % 360, point2);
		}
		
		if (scale == 1)
			bpPos = 0;
		turtle.jumpTo(-50, -50);
		Point ctrl1 = Point.point(-500, 225), ctrl2 = Point.point(500, -325);
		canvas.spawn().stroke(Colors.BLUE, 5).drawPoint(ctrl1).drawPoint(ctrl2);
		Point src = Point.point(-250, 0), dest = Point.point(250, 0);
		double f = scale / 360.0;
		Point p1 = src.interpolate(ctrl1, f);
		Point p2 = ctrl1.interpolate(ctrl2, f);
		Point p3 = ctrl2.interpolate(dest, f);
//			Point p3 = p1.interpolate(p2, f);
//			Point p4 = src.interpolate(p3, f);
//			Point p5 = p3.interpolate(dest, f);
//			canvas.color(Colors.GRAY).drawLine(p1, p2).drawLine(p2, p3);

		canvas.spawn().color(Colors.WHITE.opacity(.5)).drawLine(p1, p2).drawLine(p2, p3);
		Point p4 = p1.interpolate(p2, f);
		Point p5 = p2.interpolate(p3, f);
		Point p6 = p4.interpolate(p5, f);
		canvas.spawn().color(Colors.RED).drawLine(p4, p5);
		if (bezierPoints.size() > bpPos)
			bezierPoints.set(bpPos++, p6);
		else
			bezierPoints.add(p6);
		turtle.jumpTo(src).stroke(Colors.GREEN, 1).strokeOnly();

		for (Point p : bezierPoints) {
			turtle.drawTo(p);
		}
		turtle.endPath();
		canvas.spawn().stroke(Colors.GREEN, 3).drawPoint(p6);
		if (true)
			return;

//		GLLayer layer = ((GLScreen)screen).getGLLayer();
//		layer.drawImage(Point.point(0, 0), image);
		turtle.jumpTo(0, 320).turnTo(-90);
//		stem(turtle, 4);
		turtle.color(Colors.WHITE).strokeOnly();
		turtle.jumpTo(400, 150).turnTo(-90 * scale);
		drawSnowFlake(turtle, 50, 0, 3);
		turtle.jumpTo(200, -150).turnTo(-90 * scale);
		drawSnowFlake(turtle, 50, 1, 3);
		turtle.jumpTo(0, 150).turnTo(-90 * scale);
		drawSnowFlake(turtle, 50, 2, 4);
		turtle.jumpTo(-200, -150).turnTo(-90 * scale);
		drawSnowFlake(turtle, 50, 3, 3);

		turtle.jumpTo(0, 0).turnTo(0).color(Colors.GREY).draw(200);
		turtle.jumpTo(100, 100).draw(50);
		turtle.jumpTo(0, 0).turnTo(0).strokeWidth(2).color(Colors.GREEN);
//		turtle.spawn().draw(100);
//		turtle.spawn().turn(45).pen(Colors.RED).draw(100);
		Direction a = Direction.absolute(90);
		Direction b = Direction.absolute(-89);
		turtle.spawn().color(Colors.RED).turnTo(a).draw(100);
		turtle.spawn().color(Colors.GREEN).turnTo(b).draw(100);
		turtle.spawn().color(Colors.BLUE).turnTo(a.interpolate(b, .5)).draw(100);
//		turtle.spawn().color(Colors.MAGENTA).turnTo(Direction.absolute(a.dirX(), a.dirY())).draw(100);

	}

	private void drawAxes() {
		int w = (int) screen.width() / 20 * 10;
		int h = (int) screen.height() / 20 * 10;
		canvas.stroke(Colors.RED, 1);
		int l = 5;
		for (int x = -w; x <= w; x += 10) {
			if (x % 100 == 0)
				canvas.drawLine(Point.point(x, l), Point.point(x, -l));
			else
				canvas.drawPoint(Point.point(x, 0));
		}
		canvas.drawLine(Point.point(w - 10, -l), Point.point(w, 0));
		canvas.drawLine(Point.point(w - 10, l), Point.point(w, 0));
		for (int y = -h; y <= h; y += 10) {
			if (y % 100 == 0)
				canvas.drawLine(Point.point(-l, y), Point.point(l, y));
			else
				canvas.drawPoint(Point.point(0, y));
		}
		canvas.drawLine(Point.point(-l, h - 10), Point.point(0, h));
		canvas.drawLine(Point.point(l, h - 10), Point.point(0, h));
	}

	void petal(Turtle turtle, int a, double s) {
		turtle.turn(-a);
		for (int i = a; i < 360 - a; i++) {
			turtle.draw(s).turn(1);
			if (i == 180)
				turtle.turn(a);
		}
	}

	void flower(Turtle turtle) {
		turtle.fill(Colors.YELLOW.perturb()).stroke(Colors.TRANSPARENT);
		for (int i = 0; i < 5; i++) {
			petal(turtle.turn(72).spawn().jump(1), 65, .25);
			turtle.spawn().stroke(Colors.MAROON, 1).draw(5);
		}
	}

	void stem(Turtle turtle, int n) {
		turtle.stroke(Colors.GREEN).fill(Colors.TRANSPARENT);
		if (n <= 0) {
			return;
		}
		turtle.strokeWidth(n);
		turtle.draw(45 * n);
		if (n > 1) {
			stem(turtle.spawn().turn(-45), n - 2);
			stem(turtle.spawn().turn(1/* rand(-5, 5) */), n - 1);
			stem(turtle.spawn().turn(45), n - 1);
		} else {
			flower(turtle.spawn());
		}
	}

	public Screen getScreen() {
		return screen;
	}

	@Override
	public void bigStep(double deltaTime) {
//		screen.clear();
		if (scale < 1)
			scale = 1;
		drawFrame();
//		redraw();
		scale = scale + 0.5;
		if (scale > 360)
			scale = 0;
		fieldCount++;
		frameCount = fieldCount / 4;
//		redraw();
	}

	@Override
	public void smallStep(double deltaTime) {
	}

	@Override
	public void start(Screen screen) {
		demo = this;
		this.screen = screen;
		try {
			loadTiles();
		} catch (SAXException | IOException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		screen.setFullScreen(true);
		System.out.println("starting");
		image = ImageFactory.defaultFactory().imageFromResource("/turtleduck-icon.png", 256, 256, null);
		System.out.println(image);
		canvas = screen.createCanvas();
		pen = canvas.pen();
		turtle3 = canvas.turtle3();
		turtle = canvas.turtle();
	
		System.out.println("width: " + screen.width() + ", height: " + screen.height());
	}

	public void loadTiles() throws SAXException, IOException, ParserConfigurationException {
		Image sheet = new GLPixelData("/tiles_spritesheet.png");
		atlas = TileAtlas.create(sheet);
		try (InputStream stream = getClass().getResourceAsStream("/tiles_spritesheet.xml")) {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newDefaultInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.parse(stream);
			
			NodeList nodeList = document.getElementsByTagName("TextureAtlas");
			for (int i = 0; i < nodeList.getLength(); i++) {
				NodeList children = nodeList.item(i).getChildNodes();
				for (int j = 0; j < children.getLength(); j++) {
					Node n = children.item(j);
					if (n.getNodeType() == Node.ELEMENT_NODE) {
						Element texElt = (Element) n;
						if (texElt.getTagName().equals("SubTexture")) {
							String name = texElt.getAttribute("name");
							String xstr = texElt.getAttribute("x");
							String ystr = texElt.getAttribute("y");
							String widthstr = texElt.getAttribute("width");
							String heightstr = texElt.getAttribute("height");
							int x = Integer.valueOf(xstr);
							int y = Integer.valueOf(ystr);
							int width = Integer.valueOf(widthstr);
							int height = Integer.valueOf(heightstr);
							System.out.printf("%s: at %d,%d size %dx%d%n", name, x, y, width, height);
							atlas.add(name, x, y, width, height);
						}
					}
				}
			}
		}

	}

	public void drawSnowFlake(Turtle turtle, double size, int variant, int depth) {
		if (depth > 0) {
//			turtle.strokeWidth(depth);
			variant = Math.abs((variant) % 4);
//			turtle.color(c -> c.mix(Colors.TRANSPARENT, .2));
			// painter.turn(60);
			switch (variant) {
			case 0:
				for (int i = 0; i < 6; i++) {
					turtle.turn(60);
					drawSnowFlake(turtle.spawn().draw(size / 2), size / 2.5, 2, depth);
				}
				break;
			case 1:
				for (int i = 0; i < 6; i++) {
					turtle.turn(60);
					drawSnowFlake(turtle.spawn().draw(size).turn(-90), size / 1.5, variant, depth - 1);
				}
				break;
			case 2:
				turtle.turn(-90);
				for (int i = 0; i < 3; i++) {
					turtle.turn(45);
					drawSnowFlake(turtle.spawn().draw(size), size / 1.25, 3, depth - 1);
				}
				break;
			case 3:
			default:
				turtle.turn(-90);
				for (int i = 0; i < 6; i++) {
					turtle.turn(60);
					drawSnowFlake(turtle.spawn().draw(size), size / 1.5, variant, depth - 1);

				}
				break;

			}
		}
	}
}
